package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"runtime"

	"github.com/schollz/progressbar/v3"
)

var barOpts = []progressbar.Option{
	progressbar.OptionEnableColorCodes(true),
	progressbar.OptionShowIts(),
	progressbar.OptionSetWidth(60),
	progressbar.OptionUseANSICodes(true),
	progressbar.OptionShowCount(),
	progressbar.OptionSetTheme(progressbar.Theme{
		Saucer:        "[green]=[reset]",
		SaucerHead:    "[green]>[reset]",
		SaucerPadding: " ",
		BarStart:      "[",
		BarEnd:        "]",
	}),
}

const valueCount = 1_000_000

func main() {
	client := http.DefaultClient

	cores := runtime.NumCPU()

	bar := progressbar.NewOptions(valueCount, barOpts...)
	bar.Describe("Saving values... ")

	finishChan := make(chan int, cores)
	for j := 0; j <= cores; j++ {
		go func(jin int) {
			count := 0
			for i := jin * (valueCount / cores); i <= jin*(valueCount/cores)+valueCount/cores; i++ {
				id := fmt.Sprintf("k%d", i)
				reqUrl, err := url.JoinPath("http://localhost:8000", id)
				if err != nil {
					log.Fatalf("Error parsing url: %s", err.Error())
					return
				}

				req, err := http.NewRequest(http.MethodPut, reqUrl, bytes.NewBuffer([]byte(fmt.Sprintf("v%d", i))))
				if err != nil {
					log.Fatalf("Error creating request: %s", err.Error())
					return
				}

				_, err = client.Do(req)
				if err != nil {
					log.Fatalf("Error when sending request: %s", err.Error())
					return
				}
				count++
				if count%100 == 0 {
					bar.Add(100)
				}
			}
			finishChan <- 0
		}(j)
	}
	for i := 0; i <= cores; i++ {
		<-finishChan
	}
}
